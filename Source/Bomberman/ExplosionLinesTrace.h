// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "ExplosionLinesTrace.generated.h"

#define COLLISION_EXPLOSION ECC_GameTraceChannel1


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BOMBERMAN_API UExplosionLinesTrace : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UExplosionLinesTrace();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Argument")
		float Radius = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Argument")
		int LineCount = 30;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Collision")
		bool IsEnabledCollision = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosion result")
		APlayerController* InstigatorPlayerController;
};
