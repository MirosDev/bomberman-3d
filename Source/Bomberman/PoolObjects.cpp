// Fill out your copyright notice in the Description page of Project Settings.


#include "UObject/ConstructorHelpers.h"
#include "PoolObjects.h"

// Sets default values
APoolObjects::APoolObjects()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APoolObjects::BeginPlay()
{
	Super::BeginPlay();
	if (SpawnObjectClass != NULL) {
		UWorld* const World = GetWorld();
		if (World) {
			for (int i = 0; i < PoolSize; i++)
			{
				AObjectWithinPool* SpawnObject = World->SpawnActor<AObjectWithinPool>(SpawnObjectClass, FVector().ZeroVector, FRotator().ZeroRotator);
				SpawnObject->SetActive(false);
				Pool.Add(SpawnObject);
				UE_LOG(LogTemp, Warning, TEXT("Added object to the pool"));
			}
		}
	}
}

AObjectWithinPool* APoolObjects::GetPooledObject()
{
	for (AObjectWithinPool* PoolableActor : Pool) {
		if (!PoolableActor->IsActive()) {
			return PoolableActor;
		}
	}
	return nullptr;
}