// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealNetwork.h"
#include "ObjectWithinPool.h"
#include "GameFramework/Actor.h"

// Sets default values
AObjectWithinPool::AObjectWithinPool(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
}

// Called when the game starts or when spawned
void AObjectWithinPool::BeginPlay()
{
	Super::BeginPlay();
}

void AObjectWithinPool::SetActive(bool InActive)
{
	if (HasAuthority())
	{
		Active = InActive;
		SetActorHiddenInGame(!Active);
		SetActorEnableCollision(Active);

		if (Active) {
			OnActivation();
		}
		else {
			OnDeactivation();
		}
	}
}

bool AObjectWithinPool::IsActive()
{
	return Active;
}

void AObjectWithinPool::Deactive()
{
	SetActive(false);
}

void AObjectWithinPool::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AObjectWithinPool, Active);
}