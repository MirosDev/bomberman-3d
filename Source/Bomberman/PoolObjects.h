// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ObjectWithinPool.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Character.h"
#include "PoolObjects.generated.h"

UCLASS()
class BOMBERMAN_API APoolObjects : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APoolObjects();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	TArray<AObjectWithinPool*> Pool;

public:

	UFUNCTION(BlueprintCallable, Category = "Spawn")
		AObjectWithinPool* GetPooledObject();

	UPROPERTY(EditAnywhere, Category = "SpawnBlueprint")
		TSubclassOf<class AObjectWithinPool> SpawnObjectClass;

	UPROPERTY(EditAnywhere, Category = "PoolSize")
		int PoolSize = 100;
};
