// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ObjectWithinPool.generated.h"

UCLASS()
class BOMBERMAN_API AObjectWithinPool : public AActor
{
	GENERATED_BODY()
	
public:	

	AObjectWithinPool(const FObjectInitializer& ObjectInitializer);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(Replicated)
	bool Active;
	void Deactive();

public:	

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Activation")
		void OnActivation();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Activation")
		void OnDeactivation();

	UFUNCTION(BlueprintCallable, Category = "Activation")
		void SetActive(bool IsActive);

	UFUNCTION(BlueprintCallable, Category = "Activation")
		bool IsActive();
};
