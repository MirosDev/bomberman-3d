// Fill out your copyright notice in the Description page of Project Settings.

#include "Engine/Engine.h"
#include "DrawDebugHelpers.h"
#include <EngineGlobals.h>
#include "ExplosibleComponent.h"
#include "ExplosionLinesTrace.h"

// Sets default values for this component's properties
UExplosionLinesTrace::UExplosionLinesTrace()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UExplosionLinesTrace::BeginPlay()
{
	Super::BeginPlay();

}


// Called every frame
void UExplosionLinesTrace::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!IsEnabledCollision) {
		return;
	}

	FVector Start = GetComponentLocation();
	FVector ForwardVector = GetForwardVector();

	const float Degrees = 360.f / (float)LineCount;
	for (size_t i = 0; i < LineCount; i++) {
		FVector NormalLine = ForwardVector.RotateAngleAxis(Degrees * (float)i, FVector::UpVector);
		FVector End = (NormalLine * Radius) + Start;

		//DrawDebugLine(GetWorld(), Start, End, FColor::Green, false, 1, 0, 1);

		FHitResult OutHit;
		FCollisionQueryParams CollisionParams;

		if (GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, COLLISION_EXPLOSION, CollisionParams)) {
			if (OutHit.bBlockingHit && OutHit.GetActor()->GetName() != GetOwner()->GetName()) {
				if (GEngine) {

					//DrawDebugLine(GetWorld(), Start, OutHit.TraceEnd, FColor::Blue, false, 1, 0, 1.1);

					UActorComponent* ExplosibleComponent = OutHit.GetActor()->GetComponentByClass(UExplosibleComponent::StaticClass());
					UExplosibleComponent* ExplosibleLTComponent = Cast<UExplosibleComponent>(ExplosibleComponent);
					
					if (ExplosibleLTComponent != nullptr) {
						ExplosibleLTComponent->OnExplosion.Broadcast(InstigatorPlayerController, OutHit);
					}
				}
			}
		}
	}
}